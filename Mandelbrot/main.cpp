#include "./pfc_bitmap_3.h"
#include <complex>
#include <thread>
#include "thread.h"
#include "chrono.h"


using namespace std::complex_literals;


pfc::BGR_4_t* CreateLookUpTable()
{

	pfc::BGR_4_t static lookUp[32] = {
	{0, 0, 0},
	{66, 30, 15},
	{25, 7, 26},
	{9, 1, 47},
	{4, 4, 73},
	{0, 7, 100},
	{12, 44, 138},
	{57, 125, 209},
	{134, 181, 229},
	{211, 236, 248},
	{241, 233, 191},
	{248, 201, 95},
	{255, 170, 0},
	{204, 128, 0},
	{153, 87, 0},
	{116, 62, 3},
	{126, 72, 13},
	{136, 82, 23},
	{146, 92, 33},
	{156, 102, 43},
	{166, 112, 53},
	{176, 122, 63},
	{186, 132, 73},
	{196, 142, 83},
	{206, 152, 93},
	{216, 162, 103},
	{226, 172, 113},
	{236, 182, 123},
	{246, 192, 133},
	{250, 202, 143},
	{253, 212, 153},
	{255, 222, 163},
	};

	return lookUp;
}

pfc::BGR_4_t get_Color(const int iterations, const int max_iterations, pfc::BGR_4_t* lookup)
{
	if (iterations >= max_iterations)
		return lookup[0];

	int i = iterations % max_iterations;

	return lookup[i];
}



/**
 * \brief Checks if the given complex number is convergent or not
 * \param imaginary The imaginary part of the number
 * \param real The real part of the number
 * \param threshold The threshold which defines if the number is convergent or divergent
 * \param iterations The number of iterations until the convergence is checked
 * \return Ture or false if the number is convergent or not
 */
int is_pixel_convergent(const double imaginary, const double real, const size_t threshold, const int iterations)
{
	std::complex<double> zi = 0;
	std::complex<double> zn = 0;
	const std::complex<double> c(real, imaginary);

	for (size_t i = 0; i < iterations; i++)
	{
		zn = zi * zi + c;
		zi = zn;
		if (std::abs(zn) > threshold)
			return i;
	}
	return iterations;
}


/**
 * \brief The zoom factor used by changing the maximum and minimum of the real and imaginary part of the axis
 * \param real_max
 * \param real_min 
 * \param imag_max 
 * \param imag_min
 * \param point_imag
 * \param point_real
 * \param zoom_factor 
 */
void calculate_new_axis_values(double &real_max, double &real_min, double &imag_max, double &imag_min, const double point_imag, const double point_real, const double zoom_factor)
{
	real_min = point_real - (point_real-real_min) * zoom_factor;
	real_max = point_real + (real_max - point_real) * zoom_factor;
	imag_max = point_imag + (imag_max - point_imag) * zoom_factor;
	imag_min = point_imag - (point_imag - imag_min) * zoom_factor;
}


/**
 * \brief  Calculating a carpet of fractals and one fractal is calculated using multiple threads.
 * Each thread has it own part of the image to work on.
 * \param amount_of_cores The amount of threads used
 * \param bitmap_chuncks This is the height of the image for one thread (usually the height of the bitmap divided by the amount of threads)
 * \param bitMapWidth The width of the bitmap
 * \param bitMapHeight The height of the bitmap
 * \param real_max The max number of the real part
 * \param real_min The min number of the real part
 * \param imag_max The max number of the imaginary part
 * \param imag_min The min number of the imaginary part
 * \param threshold The threshold used for comparison
 * \param iterations The amount of iterations used until compared to the threshold
 * \param p_buffer_parallel The pixel buffer of the image
 */
void fractal_parallel(
	int const amount_of_cores,
	int const bitmap_chuncks,
	int const bitMapWidth,
	int const bitMapHeight,
	double real_max,
	double real_min,
	double imag_max,
	double imag_min,
	int threshold,
	int iterations,
	pfc::BGR_4_t * const p_buffer_parallel)
{

	thread_group tg;
	
	double x_help_value = 1.0 / bitMapWidth * (real_max - real_min);
	double y_help_value = 1.0 / bitMapHeight * (imag_max - imag_min);


	pfc::BGR_4_t* lookUp = CreateLookUpTable();
	for (size_t i = 0; i < amount_of_cores; i++)
	{
		int bitmap_core_height_min = i * bitmap_chuncks;
		int bitmap_core_height_max = bitmap_core_height_min + bitmap_chuncks;

		tg.add([bitMapWidth, bitmap_core_height_min, bitmap_core_height_max, x_help_value, y_help_value, real_min, imag_min, threshold, iterations, p_buffer_parallel, lookUp]
		{
			for (size_t y = bitmap_core_height_min; y < bitmap_core_height_max; y++)
			{
				for (size_t x = 0; x < bitMapWidth; x++)
				{
					double x_normalize{ x*x_help_value + real_min };
					double y_normalize{ y*y_help_value + imag_min };
					p_buffer_parallel[y * bitMapWidth + x] = {
						get_Color(is_pixel_convergent(y_normalize, x_normalize, threshold, iterations), iterations, lookUp)
					};
				}
			}
		});
	}
	tg.join_all();
}

/**
 * \brief Calculating one fractal serial - one thread for the whole fractal
 * \param bitmap_width The width of the bitmap
 * \param bitmap_height The height of the bitmap
 * \param real_max The max number of the real part
 * \param real_min The min number of the real part
 * \param imag_max The max number of the imaginary part
 * \param imag_min The min number of the imaginary part
 * \param threshold The threshold used for comparison
 * \param iterations The amount of iterations used until compared to the threshold
 * \param p_buffer_serial The pixel buffer of the image
 */
void fractal_serial(
	int const bitmap_width,
	int const bitmap_height,
	double real_max,
	double real_min,
	double imag_max,
	double imag_min,
	const int threshold,
	const int iterations,
	pfc::BGR_4_t * const p_buffer_serial)
{
	const double x_help_value = 1.0 / bitmap_width * (real_max - real_min);
	const double y_help_value = 1.0 / bitmap_height * (imag_max - imag_min);


	pfc::BGR_4_t* lookUp = CreateLookUpTable();
	for (size_t y = 0; y < bitmap_height; y++)
	{
		for (size_t x = 0; x < bitmap_width; x++)
		{
			double x_normalize{ x*x_help_value + real_min };
			double y_normalize{ y*y_help_value + imag_min };
			p_buffer_serial[y * bitmap_width + x] = {
				get_Color(is_pixel_convergent(y_normalize, x_normalize, threshold, iterations), iterations, lookUp)
			};
			
		}
	}
}


/**
 * \brief Calculating a carpet of fractals but one thread calculates one fractal.
 * All fractals are calculated parallel.
 * \param real_max The max number of the real part
 * \param real_min The min number of the real part
 * \param imag_max The max number of the imaginary part
 * \param imag_min The min number of the imaginary part
 * \param amount_of_images The amount of images to calculate
 * \param bitmap_width The width of the bitmap
 * \param bitmap_height The height of the bitmap
 * \param amount_of_cores The amount of threads used
 * \param threshold The threshold used for comparison
 * \param iterations The amount of iterations used until compared to the threshold
 * \param point_real
 * \param point_imag
 * \param zoom_factor
 */
void carpet_GPLS(
	double real_max,
	double real_min,
	double imag_max,
	double imag_min,
	int const amount_of_images,
	int const bitmap_width,
	int const bitmap_height,
	int const amount_of_cores,
	int const threshold,
	int const iterations,
	double const point_real,
	double const point_imag,
	double const zoom_factor,
	const bool saveImages = false)
{
	thread_group tg;
	int local_image = 0;
	int threads_busy = 0;
	while(local_image != amount_of_images)
	{
		if(threads_busy < amount_of_cores)
		{
			local_image++;
			tg.add([&threads_busy, bitmap_height, bitmap_width, real_max, real_min, imag_max, imag_min, threshold, iterations, local_image, saveImages]
			{
				threads_busy++;
				pfc::bitmap bmp{ bitmap_width, bitmap_height };
				auto & span{ bmp.pixel_span() };
				auto * const p_buffer_serial{ std::data(span) };
				fractal_serial(bitmap_width, bitmap_height, real_max, real_min, imag_max, imag_min, threshold,
				               iterations, p_buffer_serial);
				threads_busy--;
				if(saveImages)
					bmp.to_file("./bitmap_images_GPLS/bitmap_result" + std::to_string(local_image) + ".bmp");
			});

			calculate_new_axis_values(real_max, real_min, imag_max, imag_min, point_imag, point_real, zoom_factor);
		}
		else {
			tg.join_all();
		}
	}
}

/**
 * \brief Calculating a carpet of fractals but one fractal is calculated by multiple threads.
 * \param real_max The max number of the real part
 * \param real_min The min number of the real part
 * \param imag_max The max number of the imaginary part
 * \param imag_min The min number of the imaginary part
 * \param amount_of_images The amount of images to calculate
 * \param bitmap_width The width of the bitmap
 * \param bitmap_height The height of the bitmap
 * \param amount_of_cores The amount of threads used
 * \param threshold The threshold used for comparison
 * \param iterations The amount of iterations used until compared to the threshold
 * \param point_real
 * \param point_imag
 * \param zoom_factor
 */
void carpet_GLSP(
	double real_max, 
	double real_min, 
	double imag_max, 
	double imag_min, 
	int const amount_of_images,
	int const bitmap_width, 
	int const bitmap_height,
	int const amount_of_cores,
	int const threshold,
	int const iterations,
	double const point_real,
	double const point_imag,
	double const zoom_factor,
	const bool saveImages = false)
{
	int const bitmap_chuncks = bitmap_height / amount_of_cores;
	for (int i = 0; i < amount_of_images; ++i)
	{
		pfc::bitmap bmp{ bitmap_width, bitmap_height };
		auto & span{ bmp.pixel_span() };
		auto * const p_buffer_serial{ std::data(span) };
	
		fractal_parallel(amount_of_cores, bitmap_chuncks, bitmap_width, bitmap_height, real_max, real_min, imag_max, imag_min, threshold, iterations, p_buffer_serial);

		calculate_new_axis_values(real_max, real_min, imag_max, imag_min, point_imag, point_real, zoom_factor);

		if(saveImages)
			bmp.to_file("./bitmap_images_GLSP/bitmap_result"+ std::to_string(i) +".bmp");
	}
}

/**
 * \brief Calculating a carpet of fractals serial. So one thread does every calculation.
 * \param real_max The max number of the real part
 * \param real_min The min number of the real part
 * \param imag_max The max number of the imaginary part
 * \param imag_min The min number of the imaginary part
 * \param amount_of_images The amount of images to calculate
 * \param bitmap_width The width of the bitmap
 * \param bitmap_height The height of the bitmap
 * \param threshold The threshold used for comparison
 * \param iterations The amount of iterations used until compared to the threshold
 */
void carpet_serial(
	double real_max,
	double real_min,
	double imag_max,
	double imag_min,
	int const amount_of_images,
	int const bitmap_width,
	int const bitmap_height,
	int const threshold,
	int const iterations,
	double const point_real,
	double const point_imag,
	double const zoom_factor,
	const bool saveImages = false)
{

	for (int i = 0; i < amount_of_images; ++i)
	{
		pfc::bitmap bmp{ bitmap_width, bitmap_height };
		auto & span{ bmp.pixel_span() };
		auto * const p_buffer_serial{ std::data(span) };

		fractal_serial(bitmap_width, bitmap_height, real_max, real_min, imag_max, imag_min, threshold, iterations, p_buffer_serial);

		calculate_new_axis_values(real_max, real_min, imag_max, imag_min, point_imag, point_real, zoom_factor);

		if(saveImages)
			bmp.to_file("./bitmap_images_serial/bitmap_result" + std::to_string(i) + ".bmp");
	}
}

int main() {

	int const amount_of_images = 10;

	double point_real = -0.745289981;
	double point_imag = 0.113075003;
	double const real_max = 1.25470996;
	double const real_min = -2.74529005;
	double const imag_max = 1.23807502;
	double const imag_min = -1.01192498;

	double const zoom_factor = 0.95;


	int const iterations = 30;
	int const threshold = 4;

	int const bitmap_width = 1024;
	int const bitmap_height = 576;

	// The amount of cores available
	const int cores = std::thread::hardware_concurrency();
	std::cout << "Available cores: " << cores << std::endl;

	const int amount_of_cores = 8;

	std::cout << "Calculating GLSP" << std::endl;
	auto const duration_GLSP = timed_run(1, [&]
	{
		carpet_GLSP(real_max, real_min, imag_max, imag_min, amount_of_images, bitmap_width, bitmap_height, amount_of_cores, threshold, iterations, point_real, point_imag, zoom_factor);
	});

	std::cout << "Calculating GPLS" << std::endl;
	auto const duration_GPLS = timed_run(1, [&]
	{
		carpet_GPLS(real_max, real_min, imag_max, imag_min, amount_of_images, bitmap_width, bitmap_height, amount_of_cores, threshold, iterations, point_real, point_imag, zoom_factor);
	});

	std::cout << "Calculating Serial" << std::endl;
	auto const duration_serial = timed_run(1, [&]
	{
		carpet_serial(real_max, real_min, imag_max, imag_min, amount_of_images, bitmap_width, bitmap_height, threshold, iterations, point_real, point_imag, zoom_factor);
	});

	double duration_GLSP_calculated = duration_GLSP.count()*1.0 / (1000 * 1000 * 1000);
	double duration_GPLS_calculated = duration_GPLS.count()*1.0 / (1000 * 1000 * 1000);
	double duration_serial_calculated = duration_serial.count()*1.0 / (1000 * 1000 * 1000);

	std::cout << "Used cores: " << amount_of_cores << std::endl;
	std::cout << "Amount of images: " << amount_of_images << std::endl;
	std::cout << "Size of one image - Width: " << bitmap_width << " / Height: " << bitmap_height << std::endl;
	std::cout << "Iterations: " << iterations << std::endl;
	std::cout << "Threshold: " << threshold << std::endl;
	std::cout << "Time duration GLSP:" << duration_GLSP.count()*1.0 / (1000 * 1000 * 1000) << std::endl;
	std::cout << "Time duration GPLS:" << duration_GPLS.count()*1.0 / (1000 * 1000 * 1000) << std::endl;
	std::cout << "Time serial:" << duration_serial.count()*1.0/(1000*1000*1000) << std::endl;

	std::cout << "Speedup GLSP / Serial: " << duration_serial_calculated / duration_GLSP_calculated << std::endl;
	std::cout << "Speedup GPLS / Serial: " << duration_serial_calculated / duration_GPLS_calculated << std::endl;
	std::cout << "Speedup GLSP / GPLS: " << duration_GLSP_calculated / duration_GPLS_calculated << std::endl;

}
